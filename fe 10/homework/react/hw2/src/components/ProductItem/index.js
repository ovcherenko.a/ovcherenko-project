import React, {Component} from 'react'
import Modal from '../Modal'
import Button from '../Button'
import './style.scss'

class ProductItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            favorite: false,
            modalIsOpen: false
        }
    }

    componentDidMount() {
        let favArr = JSON.parse( localStorage.getItem('favoritesList') )
        if (Array.isArray(favArr)) {
            favArr.forEach( element => {
                if(element.vendorСode === this.props.productInfo.vendorСode) {
                    this.setState({
                        favorite: true
                    })
                }
            })
        } else {
            favArr = []
            localStorage.setItem('favoritesList', JSON.stringify(favArr))
        }

        let cartArr = JSON.parse( localStorage.getItem('cartList') )
        if (!Array.isArray(cartArr)){ 
            cartArr = [];
            localStorage.setItem('cartList', JSON.stringify(cartArr));
        }
    }

    addToCart() {
        let arr = JSON.parse(localStorage.getItem('cartList'))
        arr.push(this.props.productInfo);
        localStorage.setItem('cartList', JSON.stringify(arr))
        this.modalToggle()
    }

    favoriteToggle() {
        let arr = JSON.parse( localStorage.getItem('favoritesList') );
        if(this.state.favorite === false) {
            arr.push(this.props.productInfo)
            localStorage.setItem('favoritesList', JSON.stringify(arr))
            this.setState({
                favorite: true
            })
        }  else {
            arr.forEach((element, index)=> {
                if(element.vendorСode === this.props.productInfo.vendorСode) {
                    arr.splice(index, 1)
                    localStorage.setItem('favoritesList', JSON.stringify(arr))
                    this.setState({
                        favorite: false
                    })
                }
            })
        }
    }

    modalToggle() {
        this.setState({
            modalIsOpen: !this.state.modalIsOpen
        })
    }

    

    render() {
        let {name, price, urlPic, vendorСode, color} = this.props.productInfo;
        return(
            <>
                {this.state.modalIsOpen ? <Modal
                                            closeButton={true}
                                            text={`Do you want to add ${name} to cart?`}
                                            backgroundColor={`#f4ffea`}
                                            closeModal={this.modalToggle.bind(this)}
                                            buttons={[
                                                <Button 
                                                    key="keyOk"
                                                    text="Ok"
                                                    btnClick={this.addToCart.bind(this)}/>,
                                                <Button
                                                    key="keyCancel"
                                                    text="Cancel"
                                                    btnClick={this.modalToggle.bind(this)}
                                                    />
                                                
                                            ]}
                                                /> : null}
                <div className="card-wrapper">

                    <div className={this.state.favorite ? "favorite-icon-added" : "favorite-icon"} onClick={ this.favoriteToggle.bind(this) }>
                        <img src="./favorite-icon.png" alt="favorite icon"/>
                    </div>

                    <div className="image-wrapper">
                        <img src={urlPic} alt={name} className="product-img"/>
                    </div>
                    <p className="headline">{name}</p>
                    <p>Артикул: {vendorСode}</p>
                    <p>Цвет:</p>
                    <div className="color" style={{backgroundColor: color}}></div>
                    <p className="price">₴{price}</p>
                    <button className="add-to-cart-button" onClick={this.modalToggle.bind(this)}>Add to cart</button>
                </div>
            </>
        )
    }
}

export default ProductItem