import React, {Component} from 'react'
import ProductItem from '../ProductItem'
//import Modal from './Modal';
//import Button from './Button';
import './style.scss'


class ProductList extends Component {
    render() {
        return(
            <div className="product-list">
                <div className="header">
                    <span className="header-title">Список товаров</span>
                </div>

                <div className="main-list">
                    {this.props.productList.map(oneProduct => {
                        return <ProductItem 
                            key={oneProduct.vendorСode}
                            productInfo={oneProduct} />
                    })}
                </div>

            </div>
        )
    }
}

export default ProductList