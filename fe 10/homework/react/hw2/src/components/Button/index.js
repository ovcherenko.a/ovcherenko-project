import React from 'react'
import PropTypes from 'prop-types'
import './style.scss'

class Button extends React.Component { 
    constructor(props) {
        super(props)
        this.backgroundColorHover = 'rgba(0, 0, 0, 0.5)';
        this.state = {
            hover: false
        }
    }

    toggleHover = () => {
        this.setState({hover: !this.state.hover})
    }

    render() {
        let linkStyle;

        if(this.state.hover) {
            linkStyle = {backgroundColor: this.backgroundColorHover}
        } else {
            linkStyle = {backgroundColor: this.props.backgroundColor}
        }

        return(
            <button className="button" 
                style={linkStyle} 
                onClick={this.props.btnClick} 
                onMouseEnter={this.toggleHover} 
                onMouseLeave={this.toggleHover}>
                {this.props.text}
            </button>
        )
    }
}

Button.propTypes = {
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    btnClick: PropTypes.func
}

export default Button