import React, {Component} from 'react';
import ProductList from './ProductList'

import './style.scss';
import './fonts.css'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoaded: false
    }
  }


  componentDidMount() {
    fetch('./products.json')
      .then(response => response.json())
      .then(responseJson => this.setState({
          isLoaded: true,
          productList: responseJson
        }))
  }

  render() {
    return(
      <div className="wrapper">
        {this.state.isLoaded ? <ProductList 
          productList={this.state.productList}
        /> : (<div className="loading">Loading</div>)}
      </div>
    )
  }
}

export default App;
