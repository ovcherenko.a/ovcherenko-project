import { createStore, combineReducers } from 'redux'
// import { useReducer } from 'react';

const productsReducer = (state = [], action) => {
    let newState = [];

    switch(action.type) {
        case "ADD_PRODUCT_LIST":
            newState = [...state]
            return newState.concat(action.productList)
        case "ADD_PRODUCT_ITEM":
            newState = [...state]
            return newState.concat(action.productItem)
        case "DEL_PRODUCT_ITEM":
            newState = [...state]

            return newState
        default:
            return state
    }
}

const favoritesReducer = (state = [], action) => {
    if (state === undefined) {
        return []
    }

    switch(action.type) {

        default:
            return state
    }
}

const cartReducer = (state = [], action) => {
    if (state === undefined) {
        return []
    }

    switch(action.type) {

        default:
            return state
    }
}

const reducers = combineReducers({
    productsState: productsReducer,
    favoritesState: favoritesReducer,
    cartState: cartReducer
})

export const store = createStore(reducers);