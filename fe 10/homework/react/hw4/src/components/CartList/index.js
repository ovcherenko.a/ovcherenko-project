import React, { useEffect, useState } from 'react'
import CartItem from '../CartItem'
import './style.scss'

function CartList(props) {
    const [cartArr, setCartArr] = useState([])
    const [reload, setReload] = useState(false)

    useEffect( ()=>{
        let arr = JSON.parse( localStorage.getItem('cartList') )
        setCartArr(arr)
        setReload(false)
    }, [ reload ] )

    function reloadTrigger() {
        setReload(true)
    }

    return(
        <div className="cart-list">
            <div className="header">
                <span className="header-title">Корзина товаров</span>
            </div>

            <div className="main-list">
                {cartArr.map(oneProduct => {
                    return <CartItem
                        key={oneProduct.vendorСode+Math.random()}
                        productInfo={oneProduct}
                        reloadTrigger={reloadTrigger}/>
                })}
            </div>
        </div>
    )
}

export default CartList