import React, {useState, useEffect} from 'react'
import Modal from '../Modal'
import Button from '../Button'
import './style.scss'


function CartItem(props) {
    const [favorite, setFavorite] = useState(false);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const {name, price, urlPic, vendorСode, color} = props.productInfo;

    useEffect( () => {
        let favArr = JSON.parse( localStorage.getItem('favoritesList') )
        if (Array.isArray(favArr)) {
            favArr.forEach( element => {
                if(element.vendorСode === vendorСode) {
                    setFavorite(true)
                }
            })
        } else {
            favArr = []
            localStorage.setItem('favoritesList', JSON.stringify(favArr))
        }

        let cartArr = JSON.parse( localStorage.getItem('cartList') )
        if (!Array.isArray(cartArr)){ 
            cartArr = [];
            localStorage.setItem('cartList', JSON.stringify(cartArr));
        }
    }, [ ])


    function modalToggle() {
        setModalIsOpen( !modalIsOpen )
    }

    function addToCart() {
        let arr = JSON.parse(localStorage.getItem('cartList'))
        arr.push(props.productInfo);
        localStorage.setItem('cartList', JSON.stringify(arr))
        modalToggle()
    }

    function toggleFavorite() {
        let arr = JSON.parse( localStorage.getItem('favoritesList') );
        if(favorite === false) {
            arr.push(props.productInfo)
            localStorage.setItem('favoritesList', JSON.stringify(arr))
            setFavorite(true)
        }  else {
            arr.forEach((element, index)=> {
                if(element.vendorСode === props.productInfo.vendorСode) {
                    arr.splice(index, 1)
                    localStorage.setItem('favoritesList', JSON.stringify(arr))
                    setFavorite(false)
                }
            })
        }
        props.reloadTrigger()
    }

    return(
        <>
        {modalIsOpen ? <Modal
                            closeButton={true}
                            text={`Do you want to add ${name} to cart?`}
                            backgroundColor={`#f4ffea`}
                            closeModal={modalToggle.bind(this)}
                            buttons={[
                                <Button 
                                    key="keyOk"
                                    text="Ok"
                                    btnClick={addToCart.bind(this)}/>,
                                <Button
                                    key="keyCancel"
                                    text="Cancel"
                                    btnClick={modalToggle.bind(this)}
                                    />
                                
                            ]}
                                /> : null}
        <div className="card-wrapper">

            <div className={favorite ? "favorite-icon-added" : "favorite-icon"} onClick={ toggleFavorite.bind(this) }>
                <img src="./favorite-icon.png" alt="favorite icon"/>
            </div>

            <div className="image-wrapper">
                <img src={urlPic} alt={name} className="product-img"/>
            </div>
            <p className="headline">{name}</p>
            <p>Артикул: {vendorСode}</p>
            <p>Цвет:</p>
            <div className="color" style={{backgroundColor: color}}></div>
            <p className="price">₴{price}</p>
            <button className="add-to-cart-button" onClick={modalToggle.bind(this)}>Add to cart</button>
        </div>
    </>
    )
}

export default CartItem