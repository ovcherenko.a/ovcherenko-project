import React,{ useState, useEffect} from 'react'
import FavoritesItem from '../FavoritesItem'
import './style.scss'
import { store } from '../../reducers'

function CartList(props) {

    const [favArr, setFavArr] = useState([])
    const [reload, setReload] = useState(false)
    useEffect( () => {
        let arr = JSON.parse( localStorage.getItem('favoritesList') )
        // setFavArr(arr)

        setFavArr(store.getState())
        setReload(false)
    }, [ reload ]) 

    function reloadTrigger() {
        setReload(true)
    }

    return(
        <div className="product-list">
            <div className="header">
                <span className="header-title">Избранное</span>
            </div>

            <div className="main-list">
                {favArr.map(oneProduct => {
                    return <FavoritesItem
                        key={oneProduct.vendorСode}
                        productInfo={oneProduct}
                        reloadTrigger={reloadTrigger}/>
                })}
            </div>
        </div>
    )
}

export default CartList