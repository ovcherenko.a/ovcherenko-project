import React, {useState, useEffect} from 'react'
import { connect } from 'react-redux'
import { Switch, Route, NavLink } from 'react-router-dom'
import ProductList from './ProductList'
import CartList from './CartList'
import FavoritesList from './FavoritesList'


//creating store
// import {createStore} from 'redux'
import { store } from '../reducers'

//reducers
import reducers from '../reducers'


//actions
// import actions from '../actions'


//styles
import './style.scss';
import './fonts.css'




function App() {
  const [isLoaded, setIsLoaded] = useState(false)
  const [generalState, setGeneralState] = useState([])
  
  // const store = createStore(productsReducer)

  // store.subscribe( () => {
  //   console.log(store.getState())
  // })

  useEffect( () => {
    fetch('./products.json')
      .then(response => response.json())
      .then(responseJson => {
        store.dispatch({type: 'ADD_PRODUCT_LIST',
                        productList: responseJson})
        setGeneralState( store.getState() )
        setIsLoaded(true)
      })

  }, [])

  useEffect( () => {
    console.log( generalState)
  }, [generalState])



  return (
    <div className="wrapper">
      {isLoaded ? 
      <>
        <div className="menu">
          <NavLink exact to="/" activeClassName="active">Список товаров</NavLink>
          <NavLink to="/cart" activeClassName="active">Корзина</NavLink>
          <NavLink to="/favorites" activeClassName="active">Избранное</NavLink>
        </div>
        <Switch>
          <Route exact path="/" render={() => <ProductList productList={generalState.productsState}/>}/>
          {/* <Route exact path="/" render={() => <ProductList productList={ store.getState() }/>}/> */}
          <Route path="/cart" component={CartList}/>
          <Route path="/favorites" component={FavoritesList}/>
        </Switch>
      </>
      : (<div className="loading">Loading</div>)}
    </div>
  )
}

export default connect(App);
