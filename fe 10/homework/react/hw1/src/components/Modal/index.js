import React, {Component} from 'react'
import './modal.scss'
import PropTypes from 'prop-types'
import closeIcon from './close-icon.png'

class Modal extends Component {

    closeThisModal(e) {
        if (e.target.className === "modal-wrapper" || e.target.className === "close") {
            this.props.closeModal()
        }
    }

    render() {
        console.log(this.props)
        return (
            <div className="modal-wrapper" onClick={this.closeThisModal.bind(this)}>

                <div className="modal">
                    <div className="headline-wrapper">
    
                        <span>{this.props.header}</span>
                        {this.props.closeButton ? <img className="close" alt="close" src={closeIcon}/> : null} 
                    </div>
                    <div className="main-text">
                        <p>{`${this.props.text}
                            ${this.props.textSecond ? this.props.textSecond : ''}
                            `}</p>
                    </div>
                    <div className="button-wrapper">
                        {this.props.buttons.map(item => ( 
                            item
                            ))}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    textSecond: PropTypes.string,
    buttons: PropTypes.array,
    closeModal: PropTypes.func
}


export default Modal;