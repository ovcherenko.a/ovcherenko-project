import React, {Component} from 'react';
// import {createUseStyles} from 'react-jss'
import Modal from './Modal';
import Button from './Button'
import './style.scss';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      modalIsOpen: false,
      secondModalIsOpen: false
    }
  }

  modalToggle() {
    this.setState({
      modalIsOpen: !this.state.modalIsOpen
    })
  }
  
  secondModalToggle() {
    this.setState({
      secondModalIsOpen: !this.state.secondModalIsOpen
    })
  }

  render() {
    return (
      <div className='wrapper'>

        {this.state.modalIsOpen ? <Modal 
          header={'Do you want to delete this file?'}
          closeButton={true}
          text={'Once you delete this file, it won’t be possible to undo this action.'}
          textSecond={'Are you sure you want to delete it?'}
          buttons={[
            <Button
              className={"modal-button"}
              text={'Make you cry'}
              backgroundColor={"#b3382c"}
              btnClick={this.modalToggle.bind(this)}/>,
            <Button
              className={"modal-button"}
              text={"Say goodbye"}
              backgroundColor={"#b3382c"}
              btnClick={this.modalToggle.bind(this)}/>
          ]}
          closeModal={this.modalToggle.bind(this)}
        /> : null}

        {this.state.secondModalIsOpen ? <Modal
          header={'Rick Astley'}
          closeButton={false}
          text={'Never gonna give you up'}
          textSecond={'Never gonna let you down'}
          buttons={[
            <Button
              className={"modal-button"}
              text={'Make you cry'}
              backgroundColor={"#b3382c"}
              btnClick={this.secondModalToggle.bind(this)} 
              />,
            <Button
              className={"modal-button"}
              text={"Say goodbye"}
              backgroundColor={"#b3382c"}
              btnClick={this.secondModalToggle.bind(this)} 
              />
          ]}
          closeModal={this.secondModalToggle.bind(this)}
        />: null}

        <Button 
          className="main-buttons"
          text={'Open first modal'} 
          backgroundColor={'#55b9f3'}
          btnClick={this.modalToggle.bind(this)} 
        />

        <Button 
          className="main-buttons"
          text={'Open second modal'} 
          backgroundColor={'#0e90e4'}
          btnClick={this.secondModalToggle.bind(this)}
        />
      </div>
    )
  }
}

export default App;
