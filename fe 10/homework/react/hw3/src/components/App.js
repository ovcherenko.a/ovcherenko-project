import React, {useState, useEffect} from 'react'
import { Switch, Route, NavLink } from 'react-router-dom'
import ProductList from './ProductList'
import CartList from './CartList'
import FavoritesList from './FavoritesList'

import './style.scss';
import './fonts.css'

function App() {
  const [isLoaded, setIsLoaded] = useState(false)
  const [productList, setProductList] = useState([])

  useEffect( () => {
    fetch('./products.json')
      .then(response => response.json())
      .then(responseJson => {
        setIsLoaded(true)
        setProductList(responseJson)
      })
  }, [])

  return (
    <div className="wrapper">
      {isLoaded ? 
      <>
        <div className="menu">
          <NavLink exact to="/" activeClassName="active">Список товаров</NavLink>
          <NavLink to="/cart" activeClassName="active">Корзина</NavLink>
          <NavLink to="/favorites" activeClassName="active">Избранное</NavLink>
        </div>
        <Switch>
          <Route exact path="/" render={() => <ProductList productList={productList}/>}/>
          <Route path="/cart" component={CartList}/>
          <Route path="/favorites" component={FavoritesList}/>
        </Switch>
      </>
      : (<div className="loading">Loading</div>)}
    </div>
  )
}

export default App;
