import React, {useState, useEffect} from 'react'
import Modal from '../Modal'
import Button from '../Button'
import './style.scss'


function CartItem(props) {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const {name, price, urlPic, vendorСode, color} = props.productInfo;

    useEffect( () => {
        let cartArr = JSON.parse( localStorage.getItem('cartList') )
        if (!Array.isArray(cartArr)){ 
            cartArr = [];
            localStorage.setItem('cartList', JSON.stringify(cartArr));
        }
    }, [ ])


    function modalToggle() {
        setModalIsOpen( !modalIsOpen )
    }

    function deleteFromCart() {
        let arr = JSON.parse ( localStorage.getItem('cartList') )
        console.log(arr)
        if(Array.isArray(arr)) {
            arr.forEach( (element, index) => {
                if (element.vendorСode === vendorСode) {
                    arr.splice(index, 1)
                }
            })
            JSON.stringify( localStorage.setItem('cartList', JSON.stringify(arr)))
        }
        props.reloadTrigger()
    }

    return(
        <>
        {modalIsOpen ? <Modal
                            closeButton={true}
                            text={`Do really want to delete ${name} from cart?`}
                            backgroundColor={`#f4ffea`}
                            closeModal={modalToggle.bind(this)}
                            buttons={[
                                <Button 
                                    key="keyOk"
                                    text="Ok"
                                    btnClick={deleteFromCart.bind(this)}/>,
                                <Button
                                    key="keyCancel"
                                    text="Cancel"
                                    btnClick={modalToggle.bind(this)}
                                    />
                                
                            ]}
                                /> : null}
        <div className="cartCard-wrapper">
            <div className='deleteFromCart' onClick={modalToggle}>Удалить из корзины</div>
            <div className="image-wrapper">
                <img src={urlPic} alt={name} className="product-img"/>
            </div>
            <div className="text-wrapper">
                <p className="headline">{name}</p>
                <p>Артикул: {vendorСode}</p>
                <p>Цвет:</p>
                <div className="color" style={{backgroundColor: color}}></div>
                <p className="price">₴{price}</p>
            </div>
        </div>
    </>
    )
}

export default CartItem