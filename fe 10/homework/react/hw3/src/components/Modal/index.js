import React, {Component} from 'react'
import './modal.scss'
import PropTypes from 'prop-types'
import closeIcon from './close-icon.png'


class Modal extends Component {

    modalToggle(event) {
        if(event.target.className === 'close' || event.target.className === 'modal-wrapper') {
            this.props.closeModal()
        }
    }

    render() {
        return (
            <div className="modal-wrapper" onClick={this.modalToggle.bind(this)}>
                <div className="modal" style={{backgroundColor: this.props.backgroundColor}}>
                    <div className="headline-wrapper">
    
                        <span>{this.props.header}</span>
                        {this.props.closeButton && <img  
                            className="close" 
                            alt="close"
                            src={closeIcon}
                            />} 
                    </div>
                    <div className="main-text">
                        <p>{`${this.props.text}
                            ${this.props.textSecond ? this.props.textSecond : ''}`}</p>
                    </div>
                    <div className="button-wrapper">
                        {this.props.buttons}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    textSecond: PropTypes.string,
    buttons: PropTypes.array,
    closeModal: PropTypes.func
}


export default Modal;