import React from 'react'
import ProductItem from '../ProductItem'
import './style.scss'

function ProductList(props) {

    return(
        <div className="product-list">
            <div className="header">
                <span className="header-title">Список товаров</span>
            </div>

            <div className="main-list">
                {props.productList.map(oneProduct => {
                    return <ProductItem
                        key={oneProduct.vendorСode}
                        productInfo={oneProduct}/>
                })}
            </div>
        </div>
    )
}

export default ProductList