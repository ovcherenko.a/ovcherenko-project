



/**
 * Класс, объекты которого описывают параметры гамбургера. 
 * 
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException('no size given');
        } else if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
            throw new HamburgerException(`invalid size ${size}`)
        }

        if(stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && stuffing !== Hamburger.STUFFING_POTATO) {
            throw new HamburgerException(`invalid size ${stuffing}`);
        }

        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];

    } catch (e) {
        console.log(e);
    };
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 20,
    callories: 40};
Hamburger.SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 50,
    callories: 100};
Hamburger.STUFFING_CHEESE = {
    name: 'STUFFING_CHEESE',
    price: 10,
    callories: 20};
Hamburger.STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 20,
    callories: 5};
Hamburger.STUFFING_POTATO = {
    name: 'STUFFING_POTATO',
    price: 15,
    callories: 10};
Hamburger.TOPPING_MAYO = {
    name: 'TOPPING_MAYO',
    price: 20,
    callories: 5}
Hamburger.TOPPING_SPICE = {
    name: 'TOPPING_SPICE',
    price: 15,
    callories: 0}


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 * 
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function(topping) {
    try {
        if (this.toppings.includes(topping)) {
            throw new HamburgerException(`duplicate topping ${topping}`);
        } 
        
        this.toppings.push(topping)
        
    } catch (e) {
        console.log(e);
    };
};



/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function(topping) {
    try {
        if (!this.toppings.includes(topping)) {
            throw new HamburgerException('There is no topping like ' + topping);
        } 
        
        this.toppings.splice([this.toppings.indexOf(topping)], 1)
        
    } catch (e) {
        console.log(e);
    };
}


/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function() {
    return this.toppings;

};


/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function() {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function() {
    return this.stuffing;
};


/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */

Hamburger.prototype.calculatePrice = function() {
    return this.size.price + this.stuffing.price + this.toppings.reduce(function(sum, current){
return sum + current.price;
    }, 0);
};


/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function() {
    return this.size.callories + this.stuffing.callories + this.toppings.reduce(function(sum, current){
        return sum + current.callories;
            }, 0);
};


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message) {
    this.message = message;
};



// var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
