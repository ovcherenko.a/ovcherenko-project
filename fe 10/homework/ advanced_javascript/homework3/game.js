class Game {
    constructor() {
        this.cell = [],
        this.wrapper = document.createElement('div');
        this.H1 = document.createElement('h1');
        this.H3 = document.createElement('h3');
        this.table = document.createElement('div');
        this.difficulty = document.createElement('div');
        this.difficultyEasy = document.createElement('div');
        this.difficultyNorm = document.createElement('div');
        this.difficultyHard = document.createElement('div');
        this.resultBlock = document.createElement('div');
        this.restartBtn = document.createElement('div');
        this.resultTextBlock = document.createElement('h2');
        this.resultUserText = 'You won!';
        this.resultPCText = 'Computer won';
        this.H1text = 'Whack a mole';
        this.H3text = 'To start the game, select the difficulty:';
        this.timer = setInterval(() => {});
        this.nums = [];
        this.blocks = [];
        this.userScore = 0;
        this.pcScore = 0;
    }

    render() {
        this.renderWrapper();
        this.renderH1();
        this.renderH3();
        this.renderDifficulty(this.wrapper); 
        this.renderTable(this.wrapper);
        this.renderBlocks();
    }

    renderWrapper() {
        this.wrapper.classList.add('wrapper');
        document.body.appendChild(this.wrapper);
    }
    
    renderH1() {
        this.H1.innerHTML = this.H1text;
        this.wrapper.appendChild(this.H1);
    }

    renderH3() {
        this.H3.innerHTML = this.H3text;
        this.wrapper.appendChild(this.H3);
    }

    renderDifficulty(element) {
        this.difficultyEasy.innerHTML = 'easy';
        this.difficultyEasy.addEventListener('click', this.start.bind(this, 1500));
        this.difficulty.appendChild(this.difficultyEasy);
        this.difficultyNorm.innerHTML = 'norm';
        this.difficultyNorm.addEventListener('click', this.start.bind(this, 1000));
        this.difficulty.appendChild(this.difficultyNorm);
        this.difficultyHard.innerHTML = 'hard';
        this.difficultyHard.addEventListener('click', this.start.bind(this, 500));
        this.difficulty.appendChild(this.difficultyHard);
        this.difficulty.classList.add('difficulty');
        element.appendChild(this.difficulty);
    }

    renderResultBlock() {
        this.removeBlocks();
        this.resultBlock.classList.add('result');

        if (this.userScore > this.pcScore) {
            this.resultTextBlock.innerHTML = 'You won';
        } else if (this.userScore < this.pcScore) {
            this.resultTextBlock.innerHTML = 'Computer won'
        } else if (this.userScore = this.pcScore)
        this.resultTextBlock.innerHTML = 'Dead heat';
        this.resultBlock.appendChild(this.resultTextBlock);
        this.restartBtn.classList.add('restartBtn');
        this.restartBtn.innerHTML = 'restart game';
        this.restartBtn.addEventListener('click', this.restartGame.bind(this));
        this.resultBlock.appendChild(this.restartBtn);
        this.wrapper.appendChild(this.resultBlock);
    }

    removeBlocks() {
        this.H1.remove();
        this.H3.remove();
        this.difficulty.remove();
        this.table.remove();
    }

    restartGame() {
        this.timer = setInterval(() => {});
        this.nums = [];
        this.blocks = [];
        this.userScore = 0;
        this.pcScore = 0;
        this.wrapper.remove();
        this.resultBlock.remove();
        this.restartBtn.remove();
        this.resultTextBlock.remove();
        document.body.appendChild(this.wrapper);
        this.wrapper.appendChild(this.H1);
        this.wrapper.appendChild(this.H3);
        this.wrapper.appendChild(this.difficulty);
        this.wrapper.appendChild(this.table);

        for(let i = 0; i < this.cell.length; i++) {
            this.cell[i].cell.className = "";
        }

    }

    renderTable(element) {
        this.table.classList.add('table');
        element.appendChild(this.table);
    }

    renderBlocks() {
        for(let i = 0; i <= 99; ++i) {
            this.blocks.push(i);
            this.cell[i] = new Cell;
            this.cell[i].renderCell(this.table)
        }
    }

    start(time) {
        clearInterval(this.timer);
        this.timer = setInterval(() => {
            let numCell = this.randomizer()
            this.cell[ numCell ].makeActive();
            setTimeout(() => {
                this.cell[ numCell ].pcWin();
            }, time);
        }, time);
    }

    stopTimer() {
        clearInterval(this.timer);
    }

    calcUserScore() {
        for(let i = 0; i < 99; i++) {
            this.userScore += this.cell[i].userScore;
        }
    }

    calcPcScore() {
        for(let i = 0; i < 99; i++) {
            this.pcScore += this.cell[i].pcScore;
        }
    }

    randomizer() {
        let num = Math.floor(Math.random() * 100);
        if(this.nums.length > 50){
            this.stopTimer();
            this.calcUserScore();
            this.calcPcScore();
            this.renderResultBlock()
        }
        if(this.nums.includes(num)){
            return this.randomizer()
        }
        this.nums.push(num);
        return num;
    }
}

class Cell {
    constructor() {
        this.cell = document.createElement('div');
        this.userScore = 0;
        this.pcScore = 0;
    }



    renderCell(element) {
        element.appendChild(this.cell);
    }

    makeActive() {
        this.cell.classList.add('active');
        this.addEvent();
    }

    userWin() {
        this.cell.classList.remove('active');
        this.cell.classList.add('userWin');
        this.userScore += 1;
    }

    pcWin(){
        if (this.cell.classList == 'userWin'){
            return;
        }
        this.cell.classList.remove('active');
        this.cell.classList.add('pcWin');
        this.pcScore += 1;
    }

    addEvent() {
        this.cell.addEventListener('click', this.userWin.bind(this));
    }
}

let game = new Game;
game.render();