class Hamburger {
    constructor(size, stuffing) {
        this._size = size;
        this._stuffing = stuffing;
        this._toppings = [Hamburger.TOPPING_SPICE];
    }

    get toppings () {
        return this._toppings;
    }

    set toppings (topping) {
        this._toppings = topping;
    }
    
    set toppings (topping) {
        try {
            if (this._toppings.includes(topping)) {
                throw new HamburgerException(`dublicate topping ${topping}`);
            }
            
            this._toppings.push(topping);
             
        } catch (e) {
            console.log(e);
        }
    }

    set removeTopping (topping) {
        try {
            if(!this.toppings.includes(topping)) {
                throw new HamburgerException('There is no toppings like ' + topping);
            }

            this.toppings.splice([this.toppings.indexOf(topping)], 1);
            
        } catch(e) {
            console.log(e);
        }
    }

    set size(sizes) {
        this._size = sizes;
    }

    get size () {
        return this._size;
    }

    set stuffing(stuffing) {
        this._stuffing = stuffing;
    }

    get stuffing () {
        return this._stuffing;
    }

    get price () {
        return this.size.price + this.stuffing.price + this.toppings.reduce(function(sum, current){
            return sum + current.price;
        }, 0);
    }

    get calories () {
        return this.size.callories + this.stuffing.callories + this.toppings.reduce(function(sum, current){
            return sum + current.callories;
        }, 0)
    }
}

Hamburger.SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 20,
    callories: 40};

Hamburger.SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 50,
    callories: 100};

Hamburger.STUFFING_CHEESE = {
    name: 'STUFFING_CHEESE',
    price: 10,
    callories: 20};

Hamburger.STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 20,
    callories: 5};

Hamburger.STUFFING_POTATO = {
    name: 'STUFFING_POTATO',
    price: 15,
    callories: 10};

Hamburger.TOPPING_MAYO = {
    name: 'TOPPING_MAYO',
    price: 20,
    callories: 5};

Hamburger.TOPPING_SPICE = {
    name: 'TOPPING_SPICE',
    price: 15,
    callories: 0};

function HamburgerException(message) {
    this.message = message;
}

let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);