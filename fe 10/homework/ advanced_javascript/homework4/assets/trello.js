class TrelloCopy {
    constructor() {
        this.wrapper = document.createElement('div');
        this.cardDeck = document.createElement('div');
        this.cardList = document.createElement('div');
        this.newCard = document.createElement('div');
        this.sortBtn = document.createElement('div')
        this.cards = [];
        this.count = 0;
    }
    render() {
        this.wrapper.classList = 'wrapper';
        this.cardDeck.classList = 'cardDeck';
        this.cardList.classList = 'cardList';
        this.newCard.classList = 'newCard';
        this.newCard.innerText = 'Добавить карточку';
        this.sortBtn.innerText = 'A->Z';
        this.sortBtn.classList = 'sortBtn';
        this.sortBtn.addEventListener('click', this.sort.bind(this))
        this.newCard.addEventListener('click', this.createNewCard.bind(this));
        document.body.appendChild(this.wrapper);
        this.wrapper.appendChild(this.cardDeck);
        this.cardDeck.appendChild(this.sortBtn);
        this.cardDeck.appendChild(this.cardList);
        this.cardDeck.appendChild(this.newCard);
    }

    sort() {
        const cardsSort = Array.from(document.getElementsByClassName('oneCard'))
        cardsSort.sort(function(a, b){
            if(a.innerText < b.innerText) {
                return -1;
            } else {
                return 1;
            }
        })
        for (let card of cardsSort) {
            this.cardList.appendChild(card);
        }
    }

    createNewCard() {
        this.cards[this.count] = new OneCard;
        this.cards[this.count].render();
        this.cardList.appendChild(this.cards[this.count].card);
       


        this.cards[this.count].card.addEventListener('dragstart', (e)=>{
                                                    e.path[0].style.opacity = '0.4'
                                                    this.draggedSrcElem = e.path[0].innerHTML;
                                                    e.dataTransfer.effectAllowed = 'move';
                                                }, false);
                                                    
        this.cards[this.count].card.addEventListener('dragenter', (e)=>{e.path[0].classList.add('over')}, false);
        this.cards[this.count].card.addEventListener('dragover', (e)=>{if(e.preventDefault){e.preventDefault()}});
        this.cards[this.count].card.addEventListener('dragleave', (e)=>{e.path[0].classList.remove('over')});
        this.cards[this.count].card.addEventListener('drop', (e)=> {if(e.stopPropagation){e.stopPropagation()}
                                                    this.droppedSrcElem = e.path[0].innerHTML;
                                                    e.path[0].innerHTML = this.draggedSrcElem;
                                                    e.path[0].classList.remove('over');

                                                return false});
        this.cards[this.count].card.addEventListener('dragend', (e)=>{
                                                    e.path[0].style.opacity = '1';
                                                    if(this.droppedSrcElem === undefined){
                                                        e.path[0].innerHTML = ""
                                                    } else {
                                                        e.path[0].innerHTML = this.droppedSrcElem;
                                                    }

                                                    });
    }
};

class OneCard {
    constructor() {
        this.card = document.createElement('div');
        this.dragSrcElem = this;
        this.draggedSrcElem = 0;
        this.droppedSrcElem = 0;
    }
    render() {
        this.card.classList = 'oneCard';
        this.card.setAttribute('draggable', 'true');
        this.card.setAttribute('contenteditable', 'true');   
    }
}

let trelloCopy = new TrelloCopy;
trelloCopy.render();


