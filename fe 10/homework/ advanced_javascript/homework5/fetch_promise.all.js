    class Request {
        constructor() {
            this.characterRequest = [];
            this.heading = document.createElement('h1');
            this.startButton = document.createElement('button');
            this.list = document.createElement('div');
            this.filmsList = [];
            this.count = 0;
            this.countSecond = 0;
        }
    
        render() {
            document.body.appendChild(this.heading);
            this.heading.innerText = 'Список фильмов серии Звездные войны:';
            document.body.appendChild(this.startButton);
            this.startButton.innerText = 'Запросить данные о фильмах'
            document.body.appendChild(this.list);
            this.list.classList = 'list';
            this.startButton.addEventListener('click', this.request.bind(this));
        }
    
    
    
        request() {
            fetch('https://swapi.co/api/films/')
                .then(response => response.json())
                .then(films => films.results)
                .then(films => { films.forEach(element => {
                                    this.filmsList[this.count] = new TextBlock ();
                                    this.filmsList[this.count].render(element.episode_id, element.title, element.opening_crawl);
                                    this.list.appendChild(this.filmsList[this.count].block);
                                    let tempFilm = this.filmsList[this.count] 
                                    this.count += 1; 
                                    let namePromises = []
                                    element.characters.forEach(element => {
                                        let namePromise = fetch(element)
                                                .then(response => response.json())
                                        namePromises.push(namePromise)
                                            }
                                    )

                                    Promise.all(namePromises)
                                        .then(elem => elem.forEach(element => {
                                            tempFilm.charactersList.innerHTML += `${element.name} </br>`;
                                        }))
                                }
                            )
                        }
                    )
                }
            }
    
    class TextBlock {
        constructor() {
            this.block = document.createElement('div')
            this.episodeIdP = document.createElement('p');
            this.titleP = document.createElement('h3');
            this.openingCrawlP = document.createElement('p')
            this.characterTitle = document.createElement('p');
            this.charactersList = document.createElement('p');
            this.characters = document.createElement('p');
    
        }
    
        render(episode_id, title, crawl) {
            document.body.appendChild(this.block);
            this.episodeIdP.innerText = episode_id;
            this.block.appendChild(this.episodeIdP);
            this.titleP.innerText = title;
            this.block.appendChild(this.titleP);
            this.openingCrawlP.innerText = crawl;
            this.block.appendChild(this.openingCrawlP);
            this.block.appendChild(this.characterTitle);
            this.characterTitle.innerText = 'Персонажи:';
            this.block.appendChild(this.charactersList);
        }
        
    }
    
    let request = new Request;
    request.render();