
class Request {
    constructor() {
        this.req = new XMLHttpRequest();
        this.heading = document.createElement('h1');
        this.startButton = document.createElement('button');
        this.list = document.createElement('div');
        this.filmsList = [];
        this.count = 0;
    }

    render() {
        document.body.appendChild(this.heading);
        this.heading.innerText = 'Список фильмов серии Звездные войны:';
        document.body.appendChild(this.startButton);
        this.startButton.innerText = 'Запросить данные о фильмах'
        document.body.appendChild(this.list);
        this.list.classList = 'list';
        this.startButton.addEventListener('click', this.request.bind(this));
    }

    request() {
        this.req.open("GET", "https://swapi.co/api/films/", true);
        this.req.send();
        this.req.onreadystatechange = () => {
            if (this.req.readyState != 4) {
                return;
            } else if (this.req.status == 200) {
                let films = JSON.parse(this.req.responseText);
                this.filmListResult = films.results;
                this.filmListResult.forEach(element => {
                    let filmsList = new TextBlock;
                    filmsList.render(element.episode_id, element.title, element.opening_crawl, element.characters, this.filmsList[this.count]);
                    this.list.appendChild(filmsList.block);
                });
            }
        }
    }
}
    


class TextBlock {
    constructor() {
        this.block = document.createElement('div')
        this.episodeIdP = document.createElement('p');
        this.titleP = document.createElement('h3');
        this.openingCrawlP = document.createElement('p')
        this.characterTitle = document.createElement('p');
        this.charactersList = document.createElement('p');
        this.characters = document.createElement('p');
        this.characterRequests = [];
        this.countSecond = 0;
    }

    render(episodeId, title, openingCrawl, characters, tempFilm) {
        document.body.appendChild(this.block);
        this.episodeIdP.innerText = episodeId;
        this.block.appendChild(this.episodeIdP);
        this.titleP.innerText = title;
        this.block.appendChild(this.titleP);
        this.openingCrawlP.innerText = openingCrawl;
        this.block.appendChild(this.openingCrawlP);
        this.block.appendChild(this.characterTitle);
        this.characterTitle.innerText = 'Персонажи:';
        this.block.appendChild(this.charactersList);

        for (let characterURL of characters) {
            let characterPromise = new Promise((resolve, reject) => {
                let request = new XMLHttpRequest;
                request.open("GET", characterURL);
                request.send()
                request.onreadystatechange = () => { 
                    if (request.readyState != 4) {
                        return
                    }
                    else if (request.status === 200) {
                        return resolve (JSON.parse(request.responseText).name)
                    }
                }
            })
            this.characterRequests.push(characterPromise);
            }
            let characterName = Promise.all(this.characterRequests);
            characterName
                .then(elem => elem.forEach((element) => 
                {this.charactersList.innerHTML += `${element} </br>`}))
                
            }
        }

let request = new Request;
request.render();