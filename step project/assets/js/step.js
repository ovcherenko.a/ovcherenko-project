/**
* @Desc Our services. Tabs
*
**/

    $('.tabs-service-item').click(function(){
        $('.tabs-service-item').removeClass('active');
        let tabData = $(this).attr('data-tab');
        $(this).addClass('active');
        $('.tabs-service-content-item').each( function(){
            $(this).css('display', 'none');
            if($(this).attr('data-tab') == tabData){
                $(this).css('display', 'inline-block');
            }
        })
    })


/**
* @Desc Our amazing work. Filter
*
**/
$('.works_filter_item').click(function() {
        $('.works_filter_item').css('color', 'var(--grey)');
        $(this).css('color', 'var(--turquoise');
        let filtersData = $(this).attr('data-tab');
        $('.works_filter_content_item').each( function(){
            $(this).fadeOut('slow');
            if($(this).attr('data-tab') == filtersData) {
                $(this).fadeIn('slow');
            } else if (filtersData == 'works_filter_content_item') {
            $(this).fadeIn('slow');
        }
        }); 
    });

/**
* @Desc Loadmore button
*
**/
$(".works_filter_content_item").slice(0, 12).show();
		if ($(".works_filter_content_item:hidden").length != 0) {
			$("#loadMore").show();
		}		
		$("#loadMore").on('click', function (e) {
            e.preventDefault();

            console.log($("#loader"));

            $("#loader").show('slow');
            setTimeout(function(){
                $('#loader').hide('slow');
            },2000);

            setTimeout(function(){
			$(".works_filter_content_item:hidden").slice(0, 12).fadeIn();
			if ($(".works_filter_content_item:hidden").length == 0) {
				$("#loadMore").fadeOut('slow');
            } else if ($(".works_filter_content_item:visible").length >= 36) {
                $("#loadMore").fadeOut('slow');
            }
            },2500);
});


/**
* @Desc What People Say About theHam ---карусель Jquery-------------------------------
*
**/

let leftBtn = $('.experts_wrapper_button_left');
let rightBtn = $('.experts_wrapper_button_right');
let elementsList = $('.carousel');

let pixelsOffset = 86;
let currentLeftValue = 0;
let elementsCount = elementsList.find('.experts_wrapper_photo').length;
let minimumOffset = - ((elementsCount - 4) * pixelsOffset);
let maximymOffset = 0;

leftBtn.click(function() {
    if (currentLeftValue != maximymOffset) {
        currentLeftValue += 86;
        elementsList.animate({ marginLeft : currentLeftValue + "px"}, 'slow');
    }
});

rightBtn.click(function() {
    if (currentLeftValue != minimumOffset) {
        currentLeftValue -= 86;
        elementsList.animate({ marginLeft : currentLeftValue + 'px'}, 'slow');
    }
});

let onePhoto = $('.experts_wrapper_photo').find('img');
let mainPhoto = $('.expert_photo').find('img');
let mainName = $('#expert_name');
let mainOccupation = $('#main_occupation');
let exprertText = $('#expert_text');

onePhoto.click(function(){
    $('.experts_wrapper_photo').css('bottom', '0');
    let photoSrc = $(this).attr('src');
    let localName = $(this).attr('data-user');
    let localOccupation = $(this).attr('data-occupation');
    let localText = $(this).attr('data-text')
    $(this).parent().css('bottom', '7px');
    parent = $(this).parent();
    
    exprertText.text(localText);
    mainPhoto.attr('src', photoSrc);
    mainName.text(localName);
    mainOccupation.text(localOccupation);
});

