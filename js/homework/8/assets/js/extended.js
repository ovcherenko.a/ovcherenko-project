
const input = document.getElementById('inputField');
const contentDiv = document.getElementsByClassName('content');


input.addEventListener("focus", onFocus);
input.addEventListener("blur", onBlur);


//Проверка ввода цифр
input.onkeypress = function(e) {
    console.log(e.keyCode);
    numb = +e.keyCode;
    if (e.keyCode > 47 && e.keyCode < 58 || e.keyCode == 45) {
        return;
    } else {
        return false;
    }
    
};

function onFocus(e) {
    input.style.outline = '3px solid #8df3db';
    // contentDiv[0].removeChild(span);
    };

function onBlur(e) {
    let span = document.getElementById('newSpan'); 
    let deniedP = document.getElementById('denied');

    if(deniedP) {
        contentDiv[0].removeChild(denied);
    }

    input.style.outline = 'none';
    const input_text = e.target;
    if(input.value > 0){

        input.style.outline = 'none';
        input.style.color = '#009673';

        
            if(!span) {
                span = document.createElement('span');
                span.id = 'newSpan';
                span.className = 'additional_info';
                contentDiv[0].insertBefore(span, input);
                span.innerHTML = `Текущая цена: ${input_text.value}`;
            } else if(span) {
                span.innerHTML = `Текущая цена: ${input_text.value}`;
            } 

            //Создание кнопки стирания элемента и обнуления ввода
        let aButton = document.getElementById('close_button');
            if(!aButton){
                let aButton = document.createElement('a');
                aButton.className = 'close_button';
                aButton.id = 'close_button'
                aButton.innerHTML = `x`;
                aButton.href = '#';
                span.appendChild(aButton);
        }

        let delButton = document.getElementById('close_button');
        // удаление элемента по нажатию на кнопку
        delButton.onclick = function() {
            contentDiv[0].removeChild(span);
            input.value = '';
        };
        // проверка отрицательного числа
    } else if (input.value < 0){
            if(span){
                contentDiv[0].removeChild(span);
            }
            input.style.outline = '3px solid #ff4200';
            denied = document.createElement('p');
            denied.id = 'denied';
            denied.innerHTML = 'Please enter correct price';
            contentDiv[0].appendChild(denied);
    }
}
