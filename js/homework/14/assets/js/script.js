$('.tabs-title').click(function(){
    $('.tabs-title').each(function(){
        $(this).removeClass('active');
    });
    
    let atrib = $(this).attr('data-tab');
    $(this).addClass('active');
    
    $('.info-tabs').each(function(){
        $(this).css('display', 'none');
        if(atrib == $(this).attr('data-tab')){
            $(this).css('display', 'block');
        };
    });
});