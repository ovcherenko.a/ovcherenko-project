document.addEventListener('DOMContentLoaded', onReady);

    let passShow = 0;
    let passShowSecond = 0;
    const input = document.getElementsByClassName('password');
    
    function onReady(){
        const firstPassButton = document.getElementById('switch');
        firstPassButton.addEventListener('click', function(){
            if(passShow == 0){
                passShow = 1;
                input[0].setAttribute('type', 'text');
                firstPassButton.setAttribute('class', 'fas fa-eye-slash icon-password');
            } else if(passShow == 1){
                passShow = 0;
                input[0].setAttribute('type', 'password');
                firstPassButton.setAttribute('class', 'fas fa-eye icon-password');
            }
        });

        const secondPassButton = document.getElementById('switchSecond');
        secondPassButton.addEventListener('click', function(){
            if(passShowSecond == 0){
                passShowSecond = 1;
                input[1].setAttribute('type','text');
                secondPassButton.setAttribute('class', 'fas fa-eye-slash icon-password');
            } else if(passShowSecond == 1){
                passShowSecond = 0;
                input[1].setAttribute('type','password');
                secondPassButton.setAttribute('class', 'fas fa-eye icon-password');
            }
        })

        btn = document.getElementById('btn');
        btn.addEventListener('click', function() {
            if (input[0].value == input[1].value){
                alert('You are welcome');
            } else if (input[0].value != input[1].value){
                alert('Нужно ввести одинаковые значения');
            }
        })
    };