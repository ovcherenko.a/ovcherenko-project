document.addEventListener('DOMContentLoaded', onReady);


function onReady(event) {
    document.onkeypress = function(e) {
        let btns = document.getElementsByTagName('button');

        console.log(e.keyCode);

        for (let i=0; i<btns.length; i++) {
            btns[i].setAttribute('class', 'btn');
        }
        switch(e.keyCode) {
            case 13:
                let enter = document.getElementById('enter');
                enter.setAttribute('class', 'btn-blue');
                break;
            case 115:
            case 83:
                let s = document.getElementById('s');
                s.setAttribute('class', 'btn-blue');
                break;
            case 101:
            case 69:
                let e = document.getElementById('e');
                e.setAttribute('class', 'btn-blue');
                break;
            case 111:
            case 79:
                let o = document.getElementById('o');
                o.setAttribute('class', 'btn-blue');
                break;
            case 110:
            case 78:
                let n = document.getElementById('n');
                n.setAttribute('class', 'btn-blue');
                break;
            case 108:
            case 76:
                let l = document.getElementById('l');
                l.setAttribute('class', 'btn-blue');
                break;
            case 122:
            case 90:
                let z = document.getElementById('z');
                z.setAttribute('class', 'btn-blue');
                break;
        }  
    }
}