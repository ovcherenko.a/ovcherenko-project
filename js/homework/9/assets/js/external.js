document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
    document.querySelector('.tabs').addEventListener('click', tabsFunc);
    function tabsFunc(event) {
        // console.log(event);
        // console.log(event.target.getAttribute('data-tab'));

        let tabs = document.getElementsByClassName('tabs-title');
        // console.log('TABS ===> ' + tabs.length);
        for (let i = 0; i < tabs.length; i++){
            tabs[i].classList.remove('active');
        }
        event.target.classList.add('active');
        let tab = event.target.getAttribute('data-tab');
        let tabContent = document.getElementsByClassName('tabcontent');
        for(let i = 0; i < tabContent.length; i++){
            if (tab == i) {
                tabContent[i].style.display = 'block';
            } else {
                tabContent[i].style.display = 'none';

            }
        }
    }
}