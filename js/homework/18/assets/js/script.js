let badmark = 0, i = 0, sumMarks = 0, student = {
    name: '',
    lastName: '',
};

student.name = prompt('Введите имя студента');
student.lastName = prompt('Введите фамилию студента');
student.table = {};
let subject;

while (true) {
    subject  = prompt('Введите название предмета');
    if (subject == null) {
        break;
    }
    student.table[subject] = prompt('Введите оценку');
}

for (let marks in student.table) {
    if (student.table[marks] < 4) {
        badmark++;
    }
    i++;
    sumMarks = sumMarks + +(student.table[marks]);
}
if (badmark == 0) {
    console.log('Студент переведен на следующий курс');
}
if (sumMarks/i > 7) {
    console.log('Студенту назначена стипендия');
}