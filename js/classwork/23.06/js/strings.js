(function () {

    /**
     * @decs Сделать первые буквы большимии
     * @param str
     */
    function firstLetterCapital(str) {
        let result = '';
        for(let i = 0; i >= str.length -1; i++) {
            if (i === 0) {
                result += str.charAt(i).toUpperCase();
            } else {
                result += str.charAt(i);
            }
        }
        return str.charAt(0).toUpperCase();
    }
})