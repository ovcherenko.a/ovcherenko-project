/**
 * @desc Функция которая считает сумму двух чисел
 * @param {Number} a
 * @param {Number} b
 * @returns {Number}
 **/

function sum(a, b) {
    return a + b;
}


/**
 * @desc Умножение двух чисел
 * @param a {Number} a
 * @param b {Number} b
 **/

function multiply(a, b) {
    //console.log('a =', a, 'b =', b);
    if (isFinite(a) && isFinite(b)) {
        return a * b;
    }


    //return a * b;

}

/**
 * @desc Подсчитывает кол-во вхождений числа в сторону
 * @param str
 * @param num
 */

function numInStr(str, num) {
    let counter = 0;

    if (!str) {
        return 0;
    }

    for (let i = 0; i < str.length; i++) {
        let s = str.charAt(i);

        if (s !== '' && +s === num) {
            counter++;
        }
    }

    return counter;
}


function diff(a, b) {
    return a-b;
}


function hockey(amountOfHits, goals) {
    if (isFinite(amountOfHits) || isFinite(goals)) {
        return 0;
    }

    return goals / amountOfHits * 100;
}